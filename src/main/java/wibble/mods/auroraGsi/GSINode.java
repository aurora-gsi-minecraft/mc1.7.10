package wibble.mods.auroraGsi;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.GuiControls;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.potion.Potion;

/** Container for the Provider data and Player data. */
public class GSINode {
    private ProviderNode provider = new ProviderNode();
    private GameNode game = new GameNode();
    private WorldNode world = new WorldNode();
    private PlayerNode player = new PlayerNode();

    public GSINode update() {
        game.update();
        world.update();
        player.update();
        return this;
    }

    /**
     * Contains data required for Aurora to be able to parse the JSON data.
     */
    private static class ProviderNode {
        private String name = "minecraft";
        private int appid = -1;
    }

    /**
     * Contains the data extracted from the game about the player.
     */
    private static class PlayerNode {
        private boolean inGame;
        private float health;
        private float maxHealth;
        private float absorption;
        private boolean isDead;
        private int armor;
        private int experienceLevel;
        private float experience;
        private int foodLevel;
        private float saturationLevel;
        private boolean isSneaking;
        private boolean isRidingHorse;
        private boolean isBurning;
        private boolean isInWater;
        private HashMap<String, Boolean> playerEffects = new HashMap<String, Boolean>();

        // Potion effects that will be added to the playerEffects map.
        private static final HashMap<String, Potion> TARGET_POTIONS;
        static {
            TARGET_POTIONS = new HashMap<String, Potion>();
            TARGET_POTIONS.put("moveSpeed", Potion.moveSpeed);
            TARGET_POTIONS.put("moveSlowdown", Potion.moveSlowdown);
            TARGET_POTIONS.put("haste", Potion.digSpeed);
            TARGET_POTIONS.put("miningFatigue", Potion.digSlowdown);
            TARGET_POTIONS.put("strength", Potion.damageBoost);
            //TARGET_POTIONS.put("instantHealth", Potion.heal);
            //TARGET_POTIONS.put("instantDamage", Potion.harm);
            TARGET_POTIONS.put("jumpBoost", Potion.jump);
            TARGET_POTIONS.put("confusion", Potion.confusion);
            TARGET_POTIONS.put("regeneration", Potion.regeneration);
            TARGET_POTIONS.put("resistance", Potion.resistance);
            TARGET_POTIONS.put("fireResistance", Potion.fireResistance);
            TARGET_POTIONS.put("waterBreathing", Potion.waterBreathing);
            TARGET_POTIONS.put("invisibility", Potion.invisibility);
            TARGET_POTIONS.put("blindness", Potion.blindness);
            TARGET_POTIONS.put("nightVision", Potion.nightVision);
            TARGET_POTIONS.put("hunger", Potion.hunger);
            TARGET_POTIONS.put("weakness", Potion.weakness);
            TARGET_POTIONS.put("poison", Potion.poison);
            TARGET_POTIONS.put("wither", Potion.wither);
        }

        private void update() {
            try {
                playerEffects.clear(); // clear before attempting to get the player else there may be values on the mainmenu

                // Attempt to get a player, and store their health and stuff
                EntityPlayerSP player = Minecraft.getMinecraft().thePlayer;
                health = player.getHealth();
                maxHealth = player.getMaxHealth();
                absorption = player.getAbsorptionAmount();
                isDead = player.isDead;
                armor = player.getTotalArmorValue();
                experienceLevel = player.experienceLevel;
                experience = player.experience;
                foodLevel = player.getFoodStats().getFoodLevel();
                saturationLevel = player.getFoodStats().getSaturationLevel();
                isSneaking = player.isSneaking();
                isRidingHorse = player.isRidingHorse();
                isBurning = player.isBurning();
                isInWater = player.isInWater();

                // Populate the player's effect map
                for (Map.Entry<String, Potion> potion : TARGET_POTIONS.entrySet())
                    playerEffects.put(potion.getKey(), player.getActivePotionEffect(potion.getValue()) != null);

                inGame = true;

            } catch (Exception ex) {
                // If this failed (I.E. could not get a player, the user is probably not in a game)
                inGame = false;
            }
        }
    }

    /**
     * Contains the data extracted from the game about the current world.
     */
    private static class WorldNode {
        private long worldTime;
        private boolean isDayTime;
        private boolean isRaining;
        private float rainStrength;
        private int dimensionID;

        private void update() {
            try {
                WorldClient world = Minecraft.getMinecraft().theWorld;
                worldTime = world.getWorldTime();
                isDayTime = world.isDaytime();
                rainStrength = world.rainingStrength;
                isRaining = world.isRaining();
                dimensionID = world.provider.dimensionId;
            } catch (Exception ignore) { }
        }
    }

    /**
     * Contains lists of any used keys and any that are conflicting.
     */
    private static class GameNode {
        private KeyBinding[] keys;
        private boolean controlsGuiOpen;
        private boolean chatGuiOpen;

        private void update() {
            controlsGuiOpen = Minecraft.getMinecraft().currentScreen instanceof GuiControls;
            chatGuiOpen = Minecraft.getMinecraft().currentScreen instanceof GuiChat;
            keys = null;
            if(controlsGuiOpen)
                keys = Minecraft.getMinecraft().gameSettings.keyBindings;
        }
    }
}
